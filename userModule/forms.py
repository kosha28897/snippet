from django import forms
class ContactForm(forms.Form):
    subject=forms.CharField(max_length=100)
    email=forms.EmailField(required=False)
    message=forms.CharField(widget=forms.Textarea)

    def clean_message(self):
        message=self.cleaned_data['message']
        n=len(message.split())
        if n<4:
            raise forms.ValidationError("not enough words")
        return message
